const BingoFoodMenu=require("../models/restaurent_menu");
const { v4: uuidv4 } = require('uuid');


const register_menu=function(req,res){
  BingoFoodMenu.create({
    item_id:uuidv4(),
    restaurant_id:req.body.restaurant_id,
    item_name: req.body.item_name,
    price: req.body.price,
    availability:req.body.availability
  })
    .then((restaurant_items) => {
      res.json(restaurant_items);
    })
    .catch((err) => res.json(err));
}
module.exports =register_menu;