const BingoFoodMenu=require("../models/restaurent_menu");

const menu_of_restaurant=function(req,res){
    BingoFoodMenu.findAll({
        where: { restaurant_id: req.params.restaurant_id },
      })
        .then((restaurant_menu) => {
          res.json(restaurant_menu);
        })
        .catch((err) => console.log(err));
    };

    module.exports=menu_of_restaurant;