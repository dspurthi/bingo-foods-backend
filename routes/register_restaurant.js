const BingoFood = require("../models/restaurent_sign_up");
const { v4: uuidv4 } = require('uuid');


const register_restaurant=function(req, res) {
    console.log(req.body);
    BingoFood.create({
      restaurant_id: uuidv4(),
      name_of_restaurant: req.body.name_of_restaurant,
      location_of_restaurant: req.body.location_of_restaurant,
      date_of_join: req.body.date_of_join
    })
      .then((restaurants) => {
        res.json(restaurants);
      })
      .catch((err) => console.log(err));
  };
  module.exports = register_restaurant;

