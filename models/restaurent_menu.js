const Sequelize = require("sequelize");

const db = require("../config/database");
const BingoFood = require("./restaurent_sign_up");


const BingoFoodMenu=db.define(
  'restaurant_items',
  {
      item_id: {
          type: Sequelize.UUID,
          primaryKey: true
        },
        restaurant_id:{
          type:Sequelize.UUID,
          references: {
            model: BingoFood,
            key: 'restaurant_id'
          }
        },
        item_name: {
          type: Sequelize.STRING,
        },
        price: {
          type: Sequelize.INTEGER,
        },
        availability: {
          type: Sequelize.INTEGER,
        },
      },
      {
        schema: "bingofoods",
        freezeTableName: true,
    
        // define the table's name
        tableName: "restaurant_items",
        timestamps: false,
    
        // If don't want createdAt
        createdAt: false,
    
        // If don't want updatedAt
        updatedAt: false,
      });
module.exports = BingoFoodMenu;
