const Sales = require("../models/sales");
const { Op } = require("sequelize");

const sales_by_dates= function(req,res){
    Sales.sum('price',{
        where:{
            date_of_purchase:{
                [Op.between]: [req.params.startDate, req.params.endDate]
            } 
        }
    })
    .then((restaurant_sales) => {
        res.json(restaurant_sales);
      })
      .catch((err) => console.log(err));
};

module.exports=sales_by_dates;