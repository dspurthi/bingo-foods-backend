const BingoFood = require("../models/restaurent_sign_up");

const find_restaurant = function (req, res) {
  BingoFood.findAll({
    where: {
      restaurant_id:req.body.restaurant_id
    }})
    .then((restaurents_data) => {
      res.json(restaurents_data);
    })
    .catch((err) => console.log(err));
};

module.exports = find_restaurant;
