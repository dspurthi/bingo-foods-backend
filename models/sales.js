const Sequelize = require("sequelize");

const db = require("../config/database");
const BingoFood = require("./restaurent_sign_up");
const BingoFoodMenu = require("./restaurent_menu");
const Orders=require("./orders");

const Sales = db.define(
  "sales",
  {
    sale_id: {
      type: Sequelize.UUID,
      primaryKey: true,
    },
    order_id: {
      type: Sequelize.UUID,
      references: {
        model: Orders,
        key: "order_id",
      },
    },
    price:{
      type:Sequelize.INTEGER
    },
    date_of_purchase:{
      type:Sequelize.STRING
    }
  },
  {
    schema: "bingofoods",
    freezeTableName: true,

    // define the table's name
    tableName: "sales",
    timestamps: false,

    // If don't want createdAt
    createdAt: false,

    // If don't want updatedAt
    updatedAt: false,
  }
);
module.exports = Sales;
