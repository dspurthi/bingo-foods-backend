const BingoFood = require("../models/restaurent_sign_up");
// const { Op } = require("sequelize");
const BingoFoodMenu = require("../models/restaurent_menu");

const delete_restaurant = function (req, res) {
  BingoFoodMenu.findAll({
    where: {  restaurant_id: req.params.restaurant_id },
  })
    .then((delete_data) => {
      BingoFoodMenu.destroy({
        where: {
          restaurant_id: req.params.restaurant_id,
        },
      });
    })
    .then((delete_data) => {
      BingoFood.destroy({
        where: {
          restaurant_id: req.params.restaurant_id,
        },
      });
    })
    .then((updated_restaurants) => {
      res.json(updated_restaurants);
    })
    .catch((err) => console.log(err));
};

module.exports = delete_restaurant;
