const express = require("express");
const router = express.Router();
const db = require("../config/database");
const { Op } = require("sequelize");
const BingoFood = require("../models/restaurent_sign_up");
const BingoFoodMenu = require("../models/restaurent_menu");
const Orders=require("../models/orders");
const Sales=require("../models/sales");
const Users=require("../models/users");

const find_restaurant = require("./find_restaurants");
const register_restaurant = require("./register_restaurant");
const update_menu = require("./update_menu");
const register_menu=require("./register_menu");
const menu_of_restaurant=require("./menu_by_restaurant");
const update_restaurant=require("./update_restaurant");
const item_by_price=require("./get_products_by_price");
const item_avl_byprice=require("./item_avl_withprice");
const item_by_all_restaurants=require("./item_by_all_restaurants");
const order_n_update_availability=require("./order_and_update");
const delete_restaurant = require("./deleterestaurant");
const sales_by_dates=require("./sales");
const user=require("./users");
const order=require("./orders");
const order_details = require("./order_details");


router.get("/restaurant_data", find_restaurant);
router.post("/signup_restaurant", register_restaurant);
router.post("/restaurant_menu",register_menu);           
router.get("/restaurant_menu/:restaurant_id", menu_of_restaurant);
router.get("/restaurant_menu/all/:item_name", item_by_all_restaurants);
router.get("/restaurant_menu/below/:price", item_by_price);
router.get("/restaurant_menu/:item_name/:price", item_avl_byprice);
router.post("/restaurant_menu/order",order_n_update_availability );
router.post("/update_menu", update_menu);
router.post("/update_restaurant", update_restaurant);
router.get("/restaurants/delete/:restaurant_id",delete_restaurant);
router.get("/sales/:startDate/:endDate",sales_by_dates);
router.post("/users",user);
router.post("/order",order);
router.get("/order_details/:id",order_details);
  

module.exports = router;
