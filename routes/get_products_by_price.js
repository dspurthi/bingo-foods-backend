const BingoFoodMenu = require("../models/restaurent_menu");
const { Op } = require("sequelize");

const item_by_price = function (req, res) {
  BingoFoodMenu.findAll({
    where: {
        price: {
            [Op.lt]: req.params.price
        },
    }})
    .then((restaurant_menu) => {
        res.json(restaurant_menu);
    })
    .catch((err) => console.log(err))

};

module.exports = item_by_price;
