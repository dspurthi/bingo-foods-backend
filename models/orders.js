const Sequelize = require("sequelize");

const db = require("../config/database");
const BingoFood = require("./restaurent_sign_up");
const BingoFoodMenu = require("./restaurent_menu");
const Users=require("./users");

const Orders = db.define(
  "orders",
  {
    order_id: {
      type: Sequelize.UUID,
      primaryKey: true,
    },
    date_of_purchase:{
      type:Sequelize.DATEONLY
    },
    user_id:{
      type:Sequelize.UUID,
      references: {
        model: Users,
        key: "user_id",
      },
    }

  },
  {
    schema: "bingofoods",
    freezeTableName: true,

    // define the table's name
    tableName: "orders",
    timestamps: false,

    // If don't want createdAt
    createdAt: false,

    // If don't want updatedAt
    updatedAt: false,
  }
);
module.exports = Orders;
