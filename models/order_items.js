const Sequelize = require("sequelize");

const db = require("../config/database");
const BingoFood = require("./restaurent_sign_up");
const BingoFoodMenu = require("./restaurent_menu");
const Orders = require("./orders");

const Order_items = db.define(
  "order_items",
  {
    order_id: {
      type: Sequelize.UUID,
      primaryKey: true,
      references: {
        model: Orders,
        key: "order_id",
      },
    },
    restaurant_id: {
      type: Sequelize.UUID,
      references: {
        model: BingoFood,
        key: "restaurant_id",
      },
    },
    item_id: {
      type: Sequelize.UUID,
      primaryKey: true,
      references: {
        model: BingoFoodMenu,
        key: "item_id",
      },
    },
    quantity:{
      type:Sequelize.INTEGER
    }
  },
  {
    schema: "bingofoods",
    freezeTableName: true,

    // define the table's name
    tableName: "order_items",
    timestamps: false,

    // If don't want createdAt
    createdAt: false,

    // If don't want updatedAt
    updatedAt: false,
  }
);
module.exports = Order_items;