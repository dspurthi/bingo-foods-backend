const BingoFoodMenu=require("../models/restaurent_menu");
const { Op } = require("sequelize");

const update_menu=function(req,res){
  BingoFoodMenu.update(
    {
      price: req.body.price,
      item_name: req.body.item_name,
      availablity:req.body.availablity
    },
    {
      where: {
          item_id: req.body.item_id 
      },
    }
  )
    .then((restaurant_menu) => {
      res.json(restaurant_menu);
    })
    .catch((err) => res.json(err));
};

module.exports=update_menu;