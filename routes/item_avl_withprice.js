const BingoFoodMenu=require("../models/restaurent_menu");
const { Op } = require("sequelize");

const item_avl_byprice = function (req, res) {
  BingoFoodMenu.findAll({
    where: {
        [Op.and]:[
            {item_name:req.params.item_name},
            {price: {
                [Op.lt]: req.params.price,
            }},
        ]    
    }})
    .then((restaurant_menu) => {
        res.json(restaurant_menu);
    })
    .catch((err) => console.log(err))

};

module.exports = item_avl_byprice;
