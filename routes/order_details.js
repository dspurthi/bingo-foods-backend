const BingoFoodMenu = require("../models/restaurent_menu");
const { Op } = require("sequelize");
const Orders = require("../models/orders");
const Order_items = require("../models/order_items");
const { v4: uuidv4 } = require("uuid");
const Sales = require("../models/sales");
const Users = require("../models/users");
const BingoFood = require("../models/restaurent_sign_up");

// find user details using order id
const order_details = function (req, res) {
  const user_data_promise = Orders.findAll({
    where: {
      order_id: req.params.id,
    },
  });

  // find user name
  user_data_promise.then((user_data) => {
    user_data.forEach((i) => {
      const user_promise = Users.findOne({
        where: {
          user_id: i.user_id,
        },
      });

      user_promise.then((user) => {
        const items_promise = Order_items.findAll({
          where: {
            order_id: req.params.id,
          },
        });

        // Find all Items n Restaurent
        items_promise.then((ordered_items) => {
          let all_promises = [];
          ordered_items.forEach((i) => {
            const { item_id } = i;
            const { restaurant_id } = i;
            const promise = BingoFoodMenu.findOne({
              where: {
                item_id: item_id,
                restaurant_id: restaurant_id,
              },
            });
            all_promises.push(promise);
          });

          // find each item is from which restaurant
          Promise.all(all_promises).then((details) => {
            const rest_promises = [];
            details.forEach((i) => {
              const promise = BingoFood.findOne({
                where: {
                  restaurant_id: i.restaurant_id,
                },
              });

              rest_promises.push(promise);
            });

            // finding the required data of item,quantity,restaurent & user

            let restaurant_name;

            Promise.all(rest_promises).then((restaurents) => {
              const just_items = details.map((d) => {
                const restaurant = restaurents.find(
                  (r) => r.restaurant_id === d.restaurant_id
                );

                restaurant_name = restaurant.name_of_restaurant;
                const quantity = ordered_items.find(
                  (o) => o.item_id == d.item_id
                ).quantity;
                const { item_name } = d;
                return {
                  item_name,
                  quantity
                };
              });

              const result = {
                user: user.first_name,
                restaurant: restaurant_name,
                items: just_items,
              }

              res.json(result);  
            });
          });
        });
      });
    });
  });
};
// };

module.exports = order_details;
