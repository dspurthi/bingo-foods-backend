const Sequelize = require("sequelize");

const db = require("../config/database");


const BingoFood = db.define(
  "restaurants",
  {
    restaurant_id: {
      type: Sequelize.UUID,
      primaryKey: true
    },
    name_of_restaurant: {
      type: Sequelize.STRING,
    },
    location_of_restaurant: {
      type: Sequelize.STRING,
    },
    date_of_join: {
      type: Sequelize.STRING,
    }
  },
  {
    schema: "bingofoods",
    freezeTableName: true,

    // define the table's name
    tableName: "restaurants",
    timestamps: false,

    // If don't want createdAt
    createdAt: false,

    // If don't want updatedAt
    updatedAt: false,
  }
);

module.exports = BingoFood;
