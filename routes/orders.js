const BingoFoodMenu = require("../models/restaurent_menu");
const { Op } = require("sequelize");
const Orders = require("../models/orders");
const Order_items = require("../models/order_items");
const { v4: uuidv4 } = require("uuid");
const Sales = require("../models/sales");
const Users=require("../models/users");

const order = function (req, res) {
  const { items } = req.body;
  const {user_id} = req.body;
  console.log(user_id)

  // We just need item ids only
  const item_ids = items.map((i) => i.id);

  // Get the items first
  BingoFoodMenu.findAll({
    where: {
      item_id: item_ids,
    },
  }).then((ordered_items) => {
    let total_price = 0;
    let update_items_promises = [];

    // Get the total price
    ordered_items.forEach((item) => {
      const quantity = items.find((i) => i.id === item.item_id).quantity;
      const post_order_availability = item.availability - quantity;
      // console.log(post_order_availability)
      total_price += item.price * quantity;

      if (post_order_availability >= 0) {
        const promise = BingoFoodMenu.update(
          {
            availability: post_order_availability,
          },
          {
            where: {
              item_id: item.item_id,
            },
          }
        );

        update_items_promises.push(promise);
      }
      else{
        res.json("items not available !!!!!")
      }
    });

    Promise.all(update_items_promises).then((values) => {
      // Create an order
      const order_promise = Orders.create({
        order_id: uuidv4(),
        date_of_purchase: new Date(),
        user_id:user_id
      });

      // Create order items
      order_promise.then((order) => {
        const records = [];
        ordered_items.forEach((item) => {
          // Prepare row
          const quantity = items.find((i) => i.id === item.item_id).quantity;
          const row = {
            order_id: order.order_id,
            restaurant_id: item.restaurant_id,
            item_id: item.item_id,
            quantity: quantity,
          };

          // Insert into records to be bulk created
          records.push(row);
        });

        // Bulk create items
        Order_items.bulkCreate(records).then((order_items) => {
          // Create a sale record
          const sales_promise = Sales.create({
            sale_id: uuidv4(),
            order_id: order.order_id,
            price: total_price,
            date_of_purchase: order.date_of_purchase,
          });

          // Return final sale made
          sales_promise.then((sale) => {
            res.json(sale);
          });
        });
      });
    });
  });
};

module.exports = order;
